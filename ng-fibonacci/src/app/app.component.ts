import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  result: Observable<any>;
  index: any;

  constructor(private http: HttpClient) {
  }

  onSubmit() {
    this.result = this.http.get(`http://localhost:8080/${this.index}`);
  }
}
