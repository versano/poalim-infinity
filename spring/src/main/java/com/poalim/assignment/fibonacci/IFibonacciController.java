package com.poalim.assignment.fibonacci;

import org.springframework.http.ResponseEntity;

public interface IFibonacciController {

	public ResponseEntity<?> fibonacciNumberByIndex(int index);
}
