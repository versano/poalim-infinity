package com.poalim.assignment.fibonacci;

public interface IFibonacciRepository {

	public long getByIndex(int index);
	
	public boolean add(int index, long value);
}
