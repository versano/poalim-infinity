package com.poalim.assignment.fibonacci.dto;

public class FibonacciResponse {

	private int index;
	private long value;
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public long getValue() {
		return value;
	}
	public void setValue(long value) {
		this.value = value;
	}
	
	
	
}
