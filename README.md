# README #

This Application combine with spring boot 2 as backend and angular 9 as frontend

### Installation ###
 1. node js and npm on you work station ( https://nodejs.org/en/download/ )
 3. angular cli ( npm install -g @angular/cli )
 2. maven
 3. java jsk 8+

 ### Build and run from command line ###

in ng-fibonacci folder run: 

ng serve.
open browser at http://localhost:4200/

in spring folder run: 

mvn clean install ( once )
java -jar target/fibonacci-0.0.1-SNAPSHOT.jar


### Application ###

this is a web application that calculate fibonacci value given an index 
and display it to the user.

for example:
given the fibonacci sequence 
1, 1, 2, 3, 5, 8...

the value for index i=4 is 3
the value for index i=6 is 8
the value for index i=10 is 55

### Assignment ###

in angular (ng-fibonacci folder) create page or component with:

1. input that except numeric value ( index )
2. button that submit the input to the server and return value.
3. label that display the value to the user
3. label that display errors

* input cannot be bigger than 20 ( display error )
* feel free to add things to client side ( validations, styles ..)


in spring boot ( spring folder ):
1. create restful api ( spring controller )
    * GET method with index param and return fibonacci value ( hint: IFibonacciController )

every index can only be calculated once ( hint: IFibonacciRepository)

* feel free to add whatever is your mind to server side ( validation, logic, cache, http errors ..)

### questions.. ###
contact me dvir.versano@poalim.co.il
